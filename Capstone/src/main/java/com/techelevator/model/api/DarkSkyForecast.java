package com.techelevator.model.api;

public class DarkSkyForecast {

	DarkSkyDataBlock daily;

	public DarkSkyDataBlock getDaily() {
		return daily;
	}

	public void setDaily(DarkSkyDataBlock daily) {
		this.daily = daily;
	}
	
}
