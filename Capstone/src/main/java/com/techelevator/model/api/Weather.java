package com.techelevator.model.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Weather {
	
	private String parkCode;
	private double lowTemp;
	private double highTemp;
	
	@JsonProperty("icon")
	private String forecast;

	
	public double getLowTemp() {
		return lowTemp;
	}
	public void setLowTemp(double lowTemp) {
		this.lowTemp = lowTemp;
	}
	public double getHighTemp() {
		return highTemp;
	}
	public void setHighTemp(double highTemp) {
		this.highTemp = highTemp;
	}
	public String getForecast() {
		return forecast;
	}
	public void setForecast(String forecast) {
		this.forecast = forecast;
	}
	public String getParkCode() {
		return parkCode;
	}
	public void setParkCode(String parkCode) {
		this.parkCode = parkCode;
	}

}
