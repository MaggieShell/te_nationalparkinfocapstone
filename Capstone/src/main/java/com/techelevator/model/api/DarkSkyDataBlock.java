package com.techelevator.model.api;

import java.util.List;

public class DarkSkyDataBlock {

	private List<DarkSkyDataPoint> data;

	public List<DarkSkyDataPoint> getData() {
		return data;
	}

	public void setData(List<DarkSkyDataPoint> data) {
		this.data = data;
	}
}
