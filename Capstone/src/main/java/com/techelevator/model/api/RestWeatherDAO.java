package com.techelevator.model.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.techelevator.model.Park;
import com.techelevator.model.api.Weather;

@Component
public class RestWeatherDAO implements WeatherApiDAO {
	
	public class DarkSkyDataPoint {
		public String icon;
		public double temperatureLow;
		public double temperatureHigh;
	}
	public class DarkSkyDataBlock {
		/* List will contain 8 days of data points */
		private List<DarkSkyDataPoint> data;
	}
	public class DarkSkyForecast {
		DarkSkyDataBlock daily;
	}

	private static final String BASE_URL = "https://api.darksky.net/forecast/";

	private RestTemplate restTemplate = new RestTemplate();
	
	@Override
	public List<Weather> getDailyWeather(String parkCode, double longitude, double latitude) {
		
		String url = BASE_URL + System.getenv("DARK_SKY_SECRET_KEY") + "/" + latitude + "," + longitude +
						"?exclude=minutely,hourly,currently,flags,alerts";
		DarkSkyForecast forecast = restTemplate.getForObject(url, DarkSkyForecast.class);
		
		List<Weather> results = new ArrayList<>();
		
		for (DarkSkyDataPoint oneDayOfDataPoints : forecast.daily.data) {
			Weather w = new Weather();
			w.setParkCode(parkCode);
			w.setForecast(oneDayOfDataPoints.icon);
			w.setHighTemp(oneDayOfDataPoints.temperatureHigh);
			w.setLowTemp(oneDayOfDataPoints.temperatureLow);
			results.add(w);
		}
		return results;
	}

}
