package com.techelevator.model.api;

import java.util.List;
import java.util.Map;

import com.techelevator.model.Park;
import com.techelevator.model.api.Weather;

public interface WeatherApiDAO {

	List<Weather> getDailyWeather(String parkCode, double longitude, double latitude);

	public Map<Weather, String> getWeatherByParkCode(Park park, String tempPreference);
}
