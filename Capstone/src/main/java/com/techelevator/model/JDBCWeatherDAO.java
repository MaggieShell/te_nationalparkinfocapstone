package com.techelevator.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class JDBCWeatherDAO implements WeatherDAO {

	private JdbcTemplate jdbcTemplate;
	private static final int TODAY_DAY_VALUE = 1;
	private static final Map<String, String> FORECAST_RECOMMENDATION_MAP = new HashMap<String, String>() {
		{
			put("snow", "Pack snowshoes!");
			put("rain", "Pack rain gear, and wear waterproof shoes!");
			put("thunderstorms", "Seek shelter, and avoid hiking on exposed ridges!");
			put("sun", "Pack sunblock!");
		}
	};

	@Autowired
	public JDBCWeatherDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public Map<Weather, String> getWeatherByParkCode(String parkCode, String tempPreference) {

		Map<Weather, String> weatherInfo = new LinkedHashMap<>();

		String sql = "SELECT parkcode, fivedayforecastvalue, low, high, forecast " 
				+ "FROM weather "
				+ "WHERE parkcode = ?; ";

		SqlRowSet results = jdbcTemplate.queryForRowSet(sql, parkCode.toUpperCase());

		while (results.next()) {
			Weather w = mapRowToWeather(results);
			String recommendation = getForecastRecommendation(w.getForecast()) + " "
					+ getTemperatureRecommendation(w.getHighTemp(), w.getLowTemp());
			if (tempPreference.equals("Celsius")) {
				int highTempInC = ((w.getHighTemp() - 32) * 5) / 9;
				w.setHighTemp(highTempInC);
				int lowTempInC = ((w.getLowTemp() - 32) * 5) / 9;
				w.setLowTemp(lowTempInC);
			}
			weatherInfo.put(w, recommendation);
		}

		return weatherInfo;
	}

	@Override
	public Weather getWeatherForToday(String parkCode, String tempPreference) {

		Weather todayWeather = new Weather();

		String sql = "SELECT parkcode, fivedayforecastvalue, low, high, forecast " + "FROM weather "
				+ "WHERE parkcode = ? AND fivedayforecastvalue = ?; ";

		SqlRowSet results = jdbcTemplate.queryForRowSet(sql, parkCode.toUpperCase(), TODAY_DAY_VALUE);

		while (results.next()) {
			todayWeather = (mapRowToWeather(results));
			if (tempPreference.equals("Celsius")) {
				int highTempInC = ((todayWeather.getHighTemp() - 32) * 5) / 9;
				todayWeather.setHighTemp(highTempInC);
				int lowTempInC = ((todayWeather.getLowTemp() - 32) * 5) / 9;
				todayWeather.setLowTemp(lowTempInC);
			}
		}
		return todayWeather;
	}

	@Override
	public String getForecastRecommendation(String forecast) {
		String result = "";
		if (FORECAST_RECOMMENDATION_MAP.containsKey(forecast)) {
			result = FORECAST_RECOMMENDATION_MAP.get(forecast);
		}
		return result;
	}

	@Override
	public String getTemperatureRecommendation(int highTemp, int lowTemp) {
		String result = "";
		if (highTemp - lowTemp > 20) {
			result = "Wear breathable layers! ";
		}

		if (highTemp > 75) {
			result += "Bring an extra gallon of water!";
		} else if (lowTemp < 20) {
			result = "DANGER! FRIGID TEMPERATURES!!";
		}

		return result;
	}

	private Weather mapRowToWeather(SqlRowSet row) {
		Weather w = new Weather();

		w.setParkCode(row.getString("parkCode").toLowerCase());
		w.setForecastDayNumber(row.getInt("fivedayforecastvalue"));
		w.setLowTemp(row.getInt("low"));
		w.setHighTemp(row.getInt("high"));
		w.setForecast(row.getString("forecast").replace(" c", "C")); //

		return w;
	}

}
