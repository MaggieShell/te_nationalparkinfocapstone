package com.techelevator.model;

import java.util.List;
import java.util.Map;

public interface WeatherDAO {

	String getForecastRecommendation(String forecast);

	String getTemperatureRecommendation(int highTemp, int lowTemp);

	Weather getWeatherForToday(String parkCode, String tempPreference);

	Map<Weather, String> getWeatherByParkCode(String parkCode, String tempPreference);
}
