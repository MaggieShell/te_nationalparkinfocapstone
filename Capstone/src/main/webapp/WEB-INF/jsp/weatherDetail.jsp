<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp" />

	
	<div class="weather-group">
	
		<c:forEach var="dailyWeather" items="${weatherMap}">
			
			<c:set var="weatherItem" value="${dailyWeather.key}"/>
			<c:set var="recommendations" value="${dailyWeather.value}"/>
	
			<div class="five-day-weather">
			<h5>Day ${weatherItem.forecastDayNumber}</h5>
				<div id="daily-weather-image">
					<c:url var="dailyWeatherImgUrl" value="/img/weather/${weatherItem.forecast}.png" />
					<img src="${dailyWeatherImgUrl}" alt="forecast image for ${weatherItem.forecast}" />
				</div>
				<div>
					<%-- This block gets the temperature preference from the controller in order show
							 the correct character (C or F) for the high and low temperatures. --%>		 
					<c:choose>
						<c:when test="${tempPreference.equals('Celsius')}">
							<c:set var="tempChar" value="C" />
						</c:when>
						<c:otherwise>
							<c:set var="tempChar" value="F" />
						</c:otherwise>
					</c:choose>						
					<span>High ${weatherItem.highTemp}&deg${tempChar} | Low ${weatherItem.lowTemp}&deg${tempChar}<br/></span>
				</div>
				<br/>
				<p class="recommendations">
					${recommendations}
				</p>
			</div>
		</c:forEach>
	</div>


<c:import url="/WEB-INF/jsp/common/footer.jsp" />